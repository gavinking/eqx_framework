package cn.knet.eqxiu.pv.ui.splash;

import android.os.Bundle;

import cn.knet.eqxiu.pv.R;
import cn.knet.eqxiu.pv.base.BaseActivity;
import cn.knet.eqxiu.pv.base.BasePresenterImpl;
import cn.knet.eqxiu.pv.ui.demo.DemoActivity;
import cn.knet.eqxiu.pv.utils.UIUtil;

public class SplashActivity extends BaseActivity {


    @Override
    protected int getRootView() {
        return R.layout.activity_splash;
    }

    @Override
    protected BasePresenterImpl createPresenter() {
        return null;
    }

    @Override
    protected void setListener() {

    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        UIUtil.postDelayed(new Runnable() {
            @Override
            public void run() {
                goActivityAndFinish(DemoActivity.class);
            }
        }, 1000);
    }
}
