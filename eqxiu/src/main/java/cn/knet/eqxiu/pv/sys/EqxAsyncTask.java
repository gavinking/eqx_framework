package cn.knet.eqxiu.pv.sys;


import cn.knet.eqxiu.pv.base.BaseView;
import cn.knet.eqxiu.pv.utils.UIUtil;

/**
 * Author:  Gavin
 * Email:   gavinking@163.com
 * Date:    2016/7/25
 * Desc:    简单的异步任务类
 */
public abstract class EqxAsyncTask<T> {

    private T t;
    private BaseView baseView;

    public EqxAsyncTask() {
        this(null);
    }

    /**
     * 自动处理ProgressBar
     * @param baseView
     */
    public EqxAsyncTask(BaseView baseView) {
        this.baseView = baseView;
    }

    public void exec() {
        if (null != baseView) baseView.showLoading();
        TaskRunnable backgroundTask = new TaskRunnable();
        ThreadManager.getPool().execute(backgroundTask);
    }

    private class TaskRunnable implements Runnable {

        @Override
        public void run() {
            t = runInBackground();
            UIUtil.post(new Runnable() {
                @Override
                public void run() {
                    onPostExecute(t);
                    if (null != baseView) baseView.dismissLoading();
                }
            });
        }
    }

    /**
     * 后台线程池执行
     *
     * @return
     */
    protected abstract T runInBackground();

    /**
     * 后台任务执行完毕后到UI线程执行
     *
     * @param t
     */

    protected abstract void onPostExecute(T t);
}
