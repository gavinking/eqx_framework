package cn.knet.eqxiu.pv.widgets;

import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import cn.knet.eqxiu.pv.R;

/**
 * 作者：王栋
 * 日期：2016/4/12.
 * 描述：加载进度
 */
public class LoadingDialog extends DialogFragment implements View.OnClickListener {

    private TextView textView;
    private String textString;

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        if (args == null) {
            return;
        }
        textString = args.getString("msg");
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        getDialog().setCanceledOnTouchOutside(false);
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.dimAmount = 0.0f;
        window.setAttributes(windowParams);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        View view = inflater.inflate(R.layout.fragment_dialog_loading, container, false);
        textView = (TextView) view.findViewById(R.id.text);
        if (textString == null || textString.equals("")) {
            textView.setVisibility(View.GONE);
        } else {
            textView.setVisibility(View.VISIBLE);
            textView.setText(textString);
        }
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onClick(View v) {

    }
}
