package cn.knet.eqxiu.pv.utils;

import android.text.TextUtils;
import android.util.Log;

import cn.knet.eqxiu.pv.BuildConfig;

/**
 * Created by EqxiuZgr on 2016/7/14.
 * 应用日志输出工具
 */
public class EqxLog {
    private static final String LOG_INFO_START = "[";
    private static final String LOG_INFO_END = "] -----> ";
    private static final char LEFT_BRACKET = '(';
    private static final String SEPARATOR = ",";
    private static final String COLON = " : ";
    private static final String POINT = "\\.";

    
    /**
     * 打印Info
     * @param tag
     * @param msg
     */
    public static void i(String tag, String msg){
        if(BuildConfig.DEBUG_MODE)
            Log.i(tag, msg);
    }

    /**
     * 打印Info
     * @param tag
     * @param msg
     * @param tr
     */
    public static void i(String tag, String msg, Throwable tr){
        if(BuildConfig.DEBUG_MODE)
            Log.i(tag, msg, tr);
    }

    /**
     * 打印Error
     * @param tag
     * @param msg
     */
    public static void e(String tag, String msg){
        if(BuildConfig.DEBUG_MODE)
            Log.e(tag, msg);
    }

    /**
     * 打印Error
     * @param tag
     * @param msg
     * @param tr
     */
    public static void e(String tag, String msg, Throwable tr){
        if(BuildConfig.DEBUG_MODE)
            Log.e(tag, msg, tr);
    }

    /**
     * 打印Debug
     * @param tag
     * @param msg
     */
    public static void d(String tag, String msg){
        if(BuildConfig.DEBUG_MODE)
            Log.d(tag, msg);
    }

    /**
     * 打印Debug
     * @param tag
     * @param msg
     * @param tr
     */
    public static void d(String tag, String msg, Throwable tr){
        if(BuildConfig.DEBUG_MODE)
            Log.d(tag, msg, tr);
    }

    /**
     * 打印Verbose
     * @param tag
     * @param msg
     * @param tr
     */
    public static void v(String tag, String msg, Throwable tr){
        if(BuildConfig.DEBUG_MODE)
            Log.v(tag, msg, tr);
    }

    /**
     * 打印Warn
     * @param tag
     * @param msg
     */
    public static void w(String tag, String msg){
        if(BuildConfig.DEBUG_MODE)
            Log.d(tag, msg);
    }

    /**
     * 打印Warn
     * @param tag
     * @param msg
     * @param tr
     */
    public static void w(String tag, String msg, Throwable tr){
        if(BuildConfig.DEBUG_MODE)
            Log.d(tag, msg, tr);
    }



    private static StackTraceElement stackTraceElement;

    public static void v(Object message){
        if(BuildConfig.DEBUG_MODE){
            stackTraceElement = getStackTraceElement();
            Log.v(getDefaultTag(stackTraceElement), getLogInfo(stackTraceElement) + message.toString());
        }
    }

    public static void v(String tag, Object message){
        if(BuildConfig.DEBUG_MODE){
            stackTraceElement = getStackTraceElement();
            if(TextUtils.isEmpty(tag)){
                tag = getDefaultTag(stackTraceElement);
                Log.v(tag, getLogInfo(stackTraceElement) + message.toString());
            } else {
                Log.v(tag, getLogInfo(stackTraceElement)+tag+COLON + message.toString());
            }
        }
    }

    public static void d(Object message){
        if(BuildConfig.DEBUG_MODE){
            stackTraceElement = getStackTraceElement();
            Log.d(getDefaultTag(stackTraceElement), getLogInfo(stackTraceElement) + message.toString());
        }
    }

    public static void d(String tag, Object message){
        if(BuildConfig.DEBUG_MODE){
            stackTraceElement = getStackTraceElement();
            if(TextUtils.isEmpty(tag)){
                tag = getDefaultTag(stackTraceElement);
                Log.d(tag, getLogInfo(stackTraceElement) + message.toString());
            } else {
                Log.d(tag, tag + COLON + getLogInfo(stackTraceElement) + message.toString());
            }
        }
    }

    public static void i(Object message){
        if(BuildConfig.DEBUG_MODE){
            stackTraceElement = getStackTraceElement();
            Log.i(getDefaultTag(stackTraceElement), getLogInfo(stackTraceElement) + message.toString());
        }
    }

    public static void i(String tag, Object message){
        if(BuildConfig.DEBUG_MODE){
            stackTraceElement = getStackTraceElement();
            if(TextUtils.isEmpty(tag)){
                tag = getDefaultTag(stackTraceElement);
                Log.i(tag, getLogInfo(stackTraceElement) + message.toString());
            } else {
                Log.i(tag, getLogInfo(stackTraceElement)+tag+COLON + message.toString());
            }
        }
    }

    public static void w(Object message){
        if(BuildConfig.DEBUG_MODE){
            stackTraceElement = getStackTraceElement();
            Log.w(getDefaultTag(stackTraceElement), getLogInfo(stackTraceElement) + message.toString());
        }
    }

    public static void w(String tag, Object message){
        if(BuildConfig.DEBUG_MODE){
            stackTraceElement = getStackTraceElement();
            if(TextUtils.isEmpty(tag)){
                tag = getDefaultTag(stackTraceElement);
                Log.w(tag, getLogInfo(stackTraceElement) + message.toString());
            } else {
                Log.w(tag, tag + COLON + getLogInfo(stackTraceElement) + message.toString());
            }
        }
    }

    public static void e(Object message){
        if(BuildConfig.DEBUG_MODE){
            stackTraceElement = getStackTraceElement();
            Log.e(getDefaultTag(stackTraceElement), getLogInfo(stackTraceElement) + message.toString());
        }
    }

    public static void e(String tag, Object message){
        if(BuildConfig.DEBUG_MODE){
            stackTraceElement = getStackTraceElement();
            if(TextUtils.isEmpty(tag)){
                tag = getDefaultTag(stackTraceElement);
                Log.e(tag, getLogInfo(stackTraceElement) + message.toString());
            } else {
                Log.e(tag, getLogInfo(stackTraceElement)+tag+COLON + message.toString());
            }
        }
    }

    /**
     * 获取 当前线程的 StackThreadElement
     * @return
     */
    private static StackTraceElement getStackTraceElement(){

        return Thread.currentThread().getStackTrace()[4];
    }

    /**
     * 获取默认的TAG名称
     * @param stackTraceElement
     * @return
     */
    private static String getDefaultTag(StackTraceElement stackTraceElement){
        return stackTraceElement.getFileName().split(POINT)[0];
    }

    /**
     * 拼接日志的输出信息
     * @param stackTraceElement
     * @return
     */
    private static String getLogInfo(StackTraceElement stackTraceElement){

        StringBuilder logInfoBuilder = new StringBuilder();

        logInfoBuilder.append(LOG_INFO_START);
        logInfoBuilder.append(stackTraceElement.toString().substring(stackTraceElement.toString().indexOf(LEFT_BRACKET)));
        logInfoBuilder.append(LOG_INFO_END);

        return logInfoBuilder.toString();
    }
}
