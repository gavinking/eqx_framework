package cn.knet.eqxiu.pv.utils;

/**
 * 作者: 张国荣
 * 日期: 2016/7/22
 * 描述: Cookie
 */
public class EqxCookieUtil {

    private static final String JSESSIONID_KEY = "jsessionid";
    private static final String DOMAIN_KEY = "cookie_domain";
    private static final String JSESSIONID_TIME = "jsessionidtime";
    private static final long SIX_DAYS = 6 * 24 *60 * 60 * 1000;

    public static String getSessionidFromPref() {
        long jsessionid_time = SpUtil.get(JSESSIONID_TIME, System.currentTimeMillis());
        if((System.currentTimeMillis() - jsessionid_time) > SIX_DAYS ) {
            return "";
        }
        return SpUtil.get(JSESSIONID_KEY, "");
    }

    public static void addSessionidToPref(String jsessionid) {
        SpUtil.put(JSESSIONID_KEY, jsessionid);
        SpUtil.put(JSESSIONID_TIME, System.currentTimeMillis());
    }

    public static String getDomainFromPref() {
        long jsessionid_time = SpUtil.get(JSESSIONID_TIME, System.currentTimeMillis());
        if( (System.currentTimeMillis() - jsessionid_time) > SIX_DAYS ) {
            return "";
        }
        return SpUtil.get(DOMAIN_KEY, "");
    }

    public static void addDomainToPref(String domain) {
        SpUtil.put(DOMAIN_KEY, domain);
        SpUtil.put(JSESSIONID_TIME, System.currentTimeMillis());
    }

}
