package cn.knet.eqxiu.pv.app;

/**
 * 作者:  金录
 * 日期:  2016/9/12
 * 描述:  应用配置相关
 */
public class AppConfig {

    /**
     * 网络请求超时时间3秒
     */
    public static int TIME_OUT = 5;
}
