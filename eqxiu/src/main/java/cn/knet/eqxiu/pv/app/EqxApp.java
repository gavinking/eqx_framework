package cn.knet.eqxiu.pv.app;

import android.app.Application;
import android.content.Context;
import android.os.Handler;

/**
 * 作者:  金录
 * 日期:  2016/9/12
 * 描述:  Application
 */
public class EqxApp extends Application {

    private static Context sAppContext;
    private static Handler sMainThreadHandler;

    @Override
    public void onCreate() {
        super.onCreate();
        sAppContext = this;
        sMainThreadHandler = new Handler();
    }

    /**
     * 获取Application Context
     *
     * @return
     */
    public static Context getAppContext() {
        return sAppContext;
    }

    /**
     * 获取主线程的handler
     *
     * @return
     */
    public static Handler getMainThreadHandler() {
        return sMainThreadHandler;
    }
}
