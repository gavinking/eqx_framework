package cn.knet.eqxiu.pv.image;

import android.widget.ImageView;
import cn.knet.eqxiu.pv.R;
import cn.knet.eqxiu.pv.app.EqxApp;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;

import java.io.File;

/**
 * 作者： june
 * 日期： 2016/10/11
 * 描述：Glide图片加载工具类
 */

public class EqxImgLoader {

    /**
     * 图片加载默认背景色
     */
    private static int[] mColors = {R.color.load_image_color0,
            R.color.load_image_color1, R.color.load_image_color2, R.color.load_image_color3,
            R.color.load_image_color4, R.color.load_image_color5, R.color.load_image_color6,
            R.color.load_image_color7, R.color.load_image_color8, R.color.load_image_color9};

    /**
     * 获取随机颜色资源
     *
     * @return
     */
    private static int getRandomRes() {
        int resIndex = (int) (Math.random() * 10);
        return mColors[resIndex];
    }

    /**
     * 本地图片加载
     *
     * @param url
     * @param iv
     */
    public static void displayLocal(String url, ImageView iv, float degrees) {
        int resColor = getRandomRes();
        Glide.with(EqxApp.getAppContext())
                .load(new File(url))
                .placeholder(resColor)
                .error(resColor)
                .into(iv);
    }

    /**
     * 网络图片加载
     *
     * @param url
     * @param iv
     */
    public static void display(String url, ImageView iv) {
        int resColor = getRandomRes();
        Glide.with(EqxApp.getAppContext())
                .load(url)
                .placeholder(resColor)
                .error(resColor)
                .into(iv);
    }

    /**
     * 这个不是在dimens中指定切割，而是在代码中指定尺寸 区别就是resize
     *
     * @param url
     * @param wight  px
     * @param height px
     * @param iv
     */
    public static void display(String url, ImageView iv, int wight, int height) {
        int resColor = getRandomRes();
        Glide.with(EqxApp.getAppContext())
                .load(url)
                .override(wight, height)
                .placeholder(resColor)
                .error(resColor)
                .into(iv);
    }

    /**
     * 加载本地图片
     */
    public static void displayRes(int resId, ImageView iv) {
        int resColor = getRandomRes();
        Glide.with(EqxApp.getAppContext())
                .load(resId)
                .placeholder(resColor)
                .error(resColor)
                .into(iv);
    }

    /**
     * 从url获取bitmap
     *
     * @param url
     * @param target
     */
    public static void getBitmap(String url, SimpleTarget target) {
        Glide.with(EqxApp.getAppContext())
                .load(url)
                .asBitmap()
                .into(target);
    }


}
