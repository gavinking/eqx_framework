package cn.knet.eqxiu.pv.model.local.db;

/**
 * 作者:  金录
 * 日期:  2016/9/23
 * 描述:  数据库存储键值对的封装
 */
public class DbItem {

    private String key;
    private String value;

    public DbItem() {

    }

    public DbItem(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
