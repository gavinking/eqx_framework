package cn.knet.eqxiu.pv.base;

/**
 * 作者:    金录
 * 日期:    2016/7/18
 * 描述:
 */
public interface BasePresenter {

    /**
     * 给Presenter设置对应的View引用
     * 在Activity#onCreate中及Fragment#onViewCreated中调用
     *
     * @param view
     */
    void attachView(BaseView view);

    /**
     * Presenter中的view引用设置为null
     * detachView在Activity#onDestroy中及Fragment#onDestroyView中调用
     */
    void detachView();

//    void detachView(boolean retainInstance);
}
