package cn.knet.eqxiu.pv.sys;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 作者:    金录
 * 日期:    2016/7/18
 * 描述:    线程池管理类
 */
public class ThreadManager {

    private static volatile ThreadPoolExecutor mPool = null;
    private static final Object mLock = new Object();

    private static volatile ThreadPoolExecutor mDownloadPool = null;
    private static final Object mDownloadLock = new Object();

    /**
     * 获取一个用于执行耗时任务的线程池
     */
    public static ExecutorService getPool() {
        if (mPool == null) {
            synchronized (mLock) {
                if (mPool == null) {
                    mPool = (ThreadPoolExecutor) Executors.newFixedThreadPool(3);
                }
            }
        }
        return mPool;
    }

    /**
     * 获取下载线程池
     */
    public static ExecutorService getDownloadPool() {
        if (mDownloadPool == null) {
            synchronized (mDownloadLock) {
                if (mDownloadPool == null) {
                    mDownloadPool = (ThreadPoolExecutor) Executors.newFixedThreadPool(5);
                }
            }
        }
        return mDownloadPool;
    }


    /**
     * 取消线程池中某个还未执行的任务
     */
    public synchronized void cancel(Runnable run) {
        if (mPool != null && !(mPool.isShutdown() || mPool.isTerminating())) {
            mPool.getQueue().remove(run);
        }
    }

    /**
     * 查看线程池中是否存在特定的任务
    */
    public synchronized boolean contains(Runnable run) {
        return mPool != null && !(mPool.isShutdown() || mPool.isTerminating()) &&
                mPool.getQueue().contains(run);
    }

    /**
     * 平滑关闭线程池
     */
    public void stop() {
        if (mPool != null && !(mPool.isShutdown() || mPool.isTerminating())) {
            mPool.shutdown();
        }
    }
}
