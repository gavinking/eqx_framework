package cn.knet.eqxiu.pv.image;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;
import android.util.LruCache;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import cn.knet.eqxiu.pv.image.cache.DiskLruCache;

/**
 * 作者:  金录
 * 日期:  2016/9/21
 * 描述:  图片缓存工具类
 */
public class EqxImgCache {
    private static final String TAG = EqxImgCache.class.getSimpleName();

    private static final int MEM_MAX_SIZE = 32 * 1024 * 1024;
    private static final int DISK_MAX_SIZE = 256 * 1024 * 1024;
    private static final String DISK_CACHE_DIR = "eqxiu";
    private static final int DISK_CACHE_INDEX = 0;
    private static final int IO_BUFFER_SIZE = 8 * 1024;
    private static final int PICTURE_WIDTH = 1008;
    private static final int PICTURE_HEIGHT = 640;

    private Context mContext;
    public static LruCache<String, Bitmap> mMemoryCache;
    private static DiskLruCache mDiskCache;
    private static EqxImgCache sImgCache;

    private EqxImgCache(Context context) {
        mContext = context;
    }

    public static void init(Context context) {
        sImgCache = new EqxImgCache(context);
        sImgCache.initDiskCache();
        sImgCache.initMemoryCache();
    }

    /**
     * 加载图片
     *
     * @param uri
     * @return
     */
    public static Bitmap loadBitmap(String uri) {
        Bitmap bitmap = loadBitmapFromMemoryCache(uri);
        if (bitmap != null) {
            return bitmap;
        }

        try {
            bitmap = loadBitmapFromDiskCache(uri);
            if (bitmap != null) {
                return bitmap;
            }
            bitmap = loadBitmapFromHttp(uri);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    /**
     * 加载缓存中的图片，缓存中没有则返回null， 不去网络加载
     *
     * @param uri
     * @return
     */
    public static Bitmap loadLocalBitmap(String uri) {
        Bitmap bitmap = loadBitmapFromMemoryCache(uri);
        if (bitmap != null) {
            return bitmap;
        }

        try {
            bitmap = loadBitmapFromDiskCache(uri);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    /**
     * 将图片添加到缓存
     *
     * @param bitmap
     * @param url
     */
    public static void addBitmapToCache(String url, Bitmap bitmap) {
        final String key = hashKeyFormUrl(url);
        addBitmapToMemoryCache(key, bitmap);
        addBitmapToDiskCache(key, bitmap);
    }

    /**
     * 将bitmap加入到缓存
     *
     * @param key
     * @param bitmap
     */
    private static void addBitmapToDiskCache(String key, Bitmap bitmap) {
        if (bitmap == null) {
            return;
        }
        try {
            if (mDiskCache != null && mDiskCache.get(key) == null) {
                DiskLruCache.Editor editor = mDiskCache.edit(key);
                if (editor != null) {
                    OutputStream outputStream = editor.newOutputStream(0);
                    bitmapToStream(bitmap, outputStream);
                    editor.commit();
                }
            }
        } catch (Exception e) {
        }
    }

    /**
     * bitmap转换为OutputStream
     *
     * @param bitmap
     * @param out
     */
    private static void bitmapToStream(Bitmap bitmap, OutputStream out) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] bytes = baos.toByteArray();
        try {
            out.write(bytes);
            out.flush();
            out.close();
        } catch (Exception e) {
        }
    }

    /**
     * 从内存缓存中加载图片
     *
     * @param url
     * @return
     */
    private static Bitmap loadBitmapFromMemoryCache(String url) {
        final String key = hashKeyFormUrl(url);
        Bitmap bitmap = getBitmapFromMemoryCache(key);
        return bitmap;
    }

    /**
     * 获取内存缓存中的图片
     *
     * @param key
     * @return
     */
    private static Bitmap getBitmapFromMemoryCache(String key) {
        return mMemoryCache.get(key);
    }

    /**
     * 添加图片到内存缓存中
     *
     * @param key
     * @param bitmap
     */
    private static void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemoryCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    /**
     * 从存储缓存中加载bitmap
     *
     * @param url
     * @return
     * @throws IOException
     */
    private static Bitmap loadBitmapFromDiskCache(String url) throws IOException {
        if (mDiskCache == null) {
            return null;
        }

        Bitmap bitmap = null;
        String key = hashKeyFormUrl(url);
        DiskLruCache.Snapshot snapshot = mDiskCache.get(key);
        if (snapshot != null) {
            FileInputStream fileInputStream = (FileInputStream) snapshot.getInputStream(DISK_CACHE_INDEX);
            FileDescriptor fileDescriptor = fileInputStream.getFD();
            bitmap = decodeSampledBitmapFromFileDescriptor(fileDescriptor);
            if (bitmap != null) {
                addBitmapToMemoryCache(key, bitmap);
            }
        }
        return bitmap;
    }

    /**
     * 从网络拉取图片并存入存储缓存
     *
     * @param url
     * @return
     * @throws IOException
     */
    private static Bitmap loadBitmapFromHttp(String url) throws IOException {
        if (mDiskCache == null) {
            return null;
        }

        String key = hashKeyFormUrl(url);
        DiskLruCache.Editor editor = mDiskCache.edit(key);
        if (editor != null) {
            OutputStream outputStream = editor.newOutputStream(DISK_CACHE_INDEX);
            if (downloadUrlToStream(url, outputStream)) {
                editor.commit();
            } else {
                editor.abort();
            }
            mDiskCache.flush();
        }

        return loadBitmapFromDiskCache(url);
    }

    /**
     * 将下载链接转换成输出流
     *
     * @param urlString
     * @param outputStream
     * @return
     */
    private static boolean downloadUrlToStream(String urlString, OutputStream outputStream) {
        HttpURLConnection urlConnection = null;
        BufferedInputStream in = null;
        BufferedOutputStream out = null;

        try {
            final URL url = new URL(urlString);
            urlConnection = (HttpURLConnection) url.openConnection();
            in = new BufferedInputStream(urlConnection.getInputStream(), IO_BUFFER_SIZE);
            out = new BufferedOutputStream(outputStream, IO_BUFFER_SIZE);
            int b;
            while ((b = in.read()) != -1) {
                out.write(b);
            }
            return true;
        } catch (IOException e) {
            Log.e(TAG, "downloadBitmap failed.: " + e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            close(in);
            close(out);
        }
        return false;
    }

    /**
     * 将url转成md5字符串
     *
     * @param url
     * @return
     */
    private static String hashKeyFormUrl(String url) {
        String cacheKey;
        try {
            final MessageDigest mDigest = MessageDigest.getInstance("MD5");
            mDigest.update(url.getBytes());
            cacheKey = bytesToHexString(mDigest.digest());
        } catch (NoSuchAlgorithmException e) {
            cacheKey = String.valueOf(url.hashCode());
        }
        return cacheKey;
    }

    private static String bytesToHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    /**
     * 初始化磁盘缓存
     */
    private void initDiskCache() {
        File diskCacheDir = getDiskCacheDir(mContext, DISK_CACHE_DIR);
        if (!diskCacheDir.exists()) {
            diskCacheDir.mkdirs();
        }
        try {
            mDiskCache = DiskLruCache.open(diskCacheDir, getAppVersion(mContext), 1, DISK_MAX_SIZE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 初始化内存缓存
     */
    private void initMemoryCache() {
        mMemoryCache = new LruCache<String, Bitmap>(MEM_MAX_SIZE) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                return bitmap.getRowBytes() * bitmap.getHeight() / 1024;
            }
        };
    }

    /**
     * 获取缓存目录
     *
     * @param context
     * @param uniqueName
     * @return
     */
    private File getDiskCacheDir(Context context, String uniqueName) {
        final String cachePath = Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())
                || !Environment.isExternalStorageRemovable() ?
                context.getExternalCacheDir().getPath() : context.getCacheDir().getPath();
        return new File(cachePath + File.separator + uniqueName);
    }

    /**
     * 获取应用版本号
     *
     * @param context
     * @return
     */
    private int getAppVersion(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return info.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return 1;
    }

    /**
     * 加载文件中的图片的压缩方法
     *
     * @param fileDescriptor
     * @return
     */
    public static Bitmap decodeSampledBitmapFromFileDescriptor(FileDescriptor fileDescriptor) {
        //依然是上个方法中的四步
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
        String type = options.outMimeType;
        options.inPreferredConfig = null != type && "image/jpeg".equals(type.trim()) ?
                Bitmap.Config.RGB_565 : Bitmap.Config.ARGB_8888;
        options.inSampleSize = calculateInSampleSize(options, PICTURE_WIDTH, PICTURE_HEIGHT);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
    }

    /**
     * 计算inSampleSize
     *
     * @param options
     * @param reqWidth
     * @param reqHeight
     * @return
     */
    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        if (reqWidth == 0 || reqHeight == 0) {
            return 1;
        }

        //获取图片的宽高
        int width = options.outWidth;
        int height = options.outHeight;
        //初始化inSampleSize
        int inSampleSize = 1;
        while (width > reqWidth || height > reqHeight) {
            width /= 2;
            height /= 2;
            inSampleSize *= 2;
        }

        //计算最大inSampleSize值是2的幂，同时保持width,height大于reqWidth,reHeight
//        if (height > reqHeight || width > reqWidth) {
//            final int halfWidth = width / 2;
//            final int halfHeight = height / 2;
//
//            while ((halfHeight / inSampleSize) >= reqHeight && (halfWidth / inSampleSize) >= reqWidth) {
//                inSampleSize *= 2;
//            }
//        }
        Log.d(TAG, "inSampleSize:" + inSampleSize);
        return inSampleSize;
    }

    /**
     * 清理内存缓存
     */
    public static void clearMemoryCache() {
        mMemoryCache.evictAll();
    }

    /**
     * 关闭Closeable对象
     *
     * @param closeable
     */
    private static void close(Closeable closeable) {
        try {
            if (closeable != null) {
                closeable.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
