package cn.knet.eqxiu.pv.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * 日期: 2016/7/20
 * 描述: 应用SharedPreference存储工具类
 */
public class SpUtil {

    private static final String PREFERENCE_NAME = "eqx_pref";
    private static SharedPreferences sp;
    private static SharedPreferences.Editor editor;

    static {
        sp = UIUtil.getContext().getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        editor = sp.edit();
    }

    public static void put(String key, String value) {
        if (null != value) editor.putString(key, value).apply();
    }

    public static void put(String key, boolean value) {
        editor.putBoolean(key, value).apply();
    }

    public static void put(String key, float value) {
        editor.putFloat(key, value).apply();
    }

    public static void put(String key, int value) {
        editor.putInt(key, value).apply();
    }

    public static void put(String key, long value) {
        editor.putLong(key, value).apply();
    }


    public static String get(String key, String defValue) {
        return sp.getString(key, defValue);
    }

    public static boolean get(String key, boolean defValue) {
        return sp.getBoolean(key, defValue);
    }

    public static float get(String key, float defValue) {
        return sp.getFloat(key, defValue);
    }

    public static int get(String key, int defValue) {
        return sp.getInt(key, defValue);
    }

    public static long get(String key, long defValue) {
        return sp.getLong(key, defValue);
    }


    private static boolean clearPreferences() {
        editor.clear();
        return editor.commit();
    }
}
