package cn.knet.eqxiu.pv.model.remote;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * 作者:  金录
 * 日期:  2016/9/23
 * 描述:
 */
public interface DemoEngine {

    /**
     * 获取商品（秀点充值）列表
     *
     * @return
     */
    @GET("m/u/list/goods?type=4&pageNo=1")
    Call<JSONObject> getGoods();
}
