package cn.knet.eqxiu.pv.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.baidu.mobstat.StatService;

import butterknife.ButterKnife;
import cn.knet.eqxiu.pv.sys.ActivitiesManager;
import cn.knet.eqxiu.pv.ui.main.MainActivity;
import cn.knet.eqxiu.pv.ui.splash.SplashActivity;
import cn.knet.eqxiu.pv.utils.UIUtil;
import cn.knet.eqxiu.pv.widgets.LoadingDialog;

/**
 * 作者:    金录
 * 日期:    2016/4/30
 * 描述:    Activity基类
 */
public abstract class BaseActivity<P extends BasePresenter> extends AppCompatActivity implements
        BaseView {

    private LoadingDialog loadingDia;
    protected Context mContext;
    protected P mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;

        preLoad();
        ActivitiesManager.addActivity(this);
        setContentView(getRootView());

        ButterKnife.bind(this);
        mPresenter = createPresenter();
        if (null != mPresenter) mPresenter.attachView(this);

        initData(savedInstanceState);
        setListener();

    }

    @Override
    protected void onDestroy() {
        if (loadingDia != null)
            loadingDia.dismiss();
        loadingDia = null;
        if (null != mPresenter) {
            mPresenter.detachView();
            mPresenter = null;
        }
        super.onDestroy();
        ActivitiesManager.removeActivity(this);
    }

    /**
     * 预加载，setContentView之前执行
     */
    protected void preLoad() {

    }

    /**
     * 获取布局id
     *
     * @return
     */
    protected abstract int getRootView();

    /**
     * 创建View对应的Presenter
     *
     * @return
     */
    protected abstract P createPresenter();

    /**
     * 设置监听
     */
    protected abstract void setListener();

    /**
     * 初始化数据
     * 根据数据设置view内容
     */
    protected abstract void initData(Bundle savedInstanceState);


    /**
     * 显示加载进度对话框
     */
    public void showLoading() {
        showLoading("加载中…");
    }


    /**
     * 显示加载进度对话框
     */
    public void showLoading(String info) {
        try {
            if (loadingDia == null) {
                loadingDia = new LoadingDialog();
            }
            Bundle bundle = new Bundle();
            bundle.putString("msg", info);
            loadingDia.setArguments(bundle);
            if (!loadingDia.isAdded()) {//DialogFragment.show()内部调用了FragmentTransaction.add()方法,在add()方法时候，先判断fragmentA.isAdded()
                loadingDia.show(getFragmentManager(), "loading");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 隐藏加载进度对话框
     */
    @Override
    public void dismissLoading() {
        try {
            if (this.isFinishing())
                return;
            if (loadingDia != null && loadingDia.isAdded())
                loadingDia.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showError(String msg) {
        UIUtil.showToast(msg);
    }

    @Override
    public void showNetError() {
        UIUtil.showToast("网络链接异常，请稍后重试");
    }

    @Override
    protected void onResume() {
        super.onResume();
        //统计
        StatService.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        StatService.onPause(this);
    }

    /**
     * 跳转到其他Activity并且finish当前Activity
     *
     * @param act
     */
    protected void goActivityAndFinish(final Class<?> act) {
        Intent intent = new Intent(this, act);
        startActivity(intent);
        finish();
    }

    /**
     * 跳转到其他Activity但不finish当前的Activity
     *
     * @param act
     */
    protected void goActivity(final Class<?> act) {
        Intent intent = new Intent(this, act);
        startActivity(intent);
    }

    /**
     * 跳转到其他Activity但不finish当前的Activity
     *
     * @param intent
     */
    protected void goActivity(final Intent intent) {
        startActivity(intent);
    }

    /**
     * 如果应用未启动则启动应用
     */
    protected void startApp() {
        if (!ActivitiesManager.isActivityExist(MainActivity.class)) {
            Intent intent = new Intent(this, SplashActivity.class);
            startActivity(intent);
        }
    }
}