package cn.knet.eqxiu.pv.model;

import cn.knet.eqxiu.pv.base.BaseModel;
import cn.knet.eqxiu.pv.model.local.cache.CacheTime;
import cn.knet.eqxiu.pv.model.remote.DemoEngine;
import cn.knet.eqxiu.pv.net.JsonCallback;
import cn.knet.eqxiu.pv.net.RequestManager;
import org.json.JSONObject;

/**
 * 作者:  金录
 * 日期:  2016/9/23
 * 描述:
 */
public class DemoModel extends BaseModel<DemoEngine> {

    @Override
    protected DemoEngine createEngine() {
        return RequestManager.createCommon(DemoEngine.class);
    }

    public void test() {
        getEngine().getGoods().enqueue(new JsonCallback(null) {
            @Override
            protected void onSuccess(JSONObject body) {
                getLocalEngine().put("key", body, CacheTime.TIME_DAY);
            }
        });
        engine.getGoods();
    }



}
