package cn.knet.eqxiu.pv.net;


import java.io.IOException;

import cn.knet.eqxiu.pv.app.EqxApp;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * 作者:    金录
 * 日期:    2016/7/18
 * 描述:    缓存处理的拦截器
 */
public class CacheControlInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {

        Request request = chain.request();
        if (!RequestManager.isNetAvailable(EqxApp.getAppContext()) && "GET".equals(request
                .method())) {
//            Logger.d("cacheControl = %s", CacheControl.FORCE_CACHE);
            request = request.newBuilder()
                    .cacheControl(CacheControl.FORCE_CACHE)
                    .build();
        }
        Response originalResponse = chain.proceed(request);
        if (RequestManager.isNetAvailable(EqxApp.getAppContext())) {
            String cacheControl;
            if ("GET".equals(request.method())) {
                //GET请求缓存6秒
                cacheControl = "max-age=6";
            } else {
                //读接口上的@Headers里的配置
                cacheControl = request.cacheControl().toString();
            }
//            Logger.d("cacheControl = %s", cacheControl);
            return originalResponse.newBuilder()
                    .header("Cache-Control", cacheControl)
                    .removeHeader("Pragma")
                    .build();
        } else {
            return originalResponse.newBuilder()
                    .header("Cache-Control", "public, only-if-cached, max-stale=2419200")
                    .removeHeader("Pragma")
                    .build();
        }
    }
}
