package cn.knet.eqxiu.pv.net;

import cn.knet.eqxiu.pv.base.BasePresenterImpl;
import cn.knet.eqxiu.pv.utils.UIUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 作者:    金录
 * 日期:    2016/7/18
 * 描述:    StringCallback
 */
public abstract class StringCallback implements Callback<String> {

    private BasePresenterImpl presenter;

    public StringCallback(BasePresenterImpl presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onResponse(Call<String> call, Response<String> response) {
        //View非Attached状态不做操作
        if(!presenter.isViewAttached()) return;
        if (response.code() / 100 == 2) {
            onSuccess(response.body());
        } else {
            switch (response.code()) {
                case 400:
                    UIUtil.showToast("错误请求,请重试");
                    break;

                case 401://授权失败
                    handleAuthFail();
                    break;

                case 408:
                    UIUtil.showToast("请求超时，请检查网络后重试…");
                    break;

                case 500:
                    UIUtil.showToast("服务器开小差了，请稍后重试…");
                    break;

                default:
                    break;
            }
            onFail(response);
        }
        onFinish();
    }

    @Override
    public void onFailure(Call<String> call, Throwable t) {
        UIUtil.showToast("网络不给力，请连接网络后重试…");
        //View非Attached状态不做操作
        if(!presenter.isViewAttached()) return;
        onFail(null);
        onFinish();
    }

    /**
     * 网络请求成功
     *
     * @param body
     */
    protected abstract void onSuccess(String body);

    /**
     * 网络请求失败
     */
    protected void onFail(Response<String> response) {
    }

    /**
     * 请求结束
     */
    protected void onFinish() {

    }


    /**
     * 授权失败
     */
    private void handleAuthFail() {

    }
}
