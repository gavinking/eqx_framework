package cn.knet.eqxiu.pv.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.concurrent.TimeUnit;

import cn.knet.eqxiu.pv.BuildConfig;
import cn.knet.eqxiu.pv.app.AppConfig;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;

/**
 * 作者: 张国荣
 * 日期: 2016/7/19
 * 描述: 获取网络请求Retrofit
 */
public class RequestManager {

    /**
     * 返回为JSON格式
     */
    public static final int CALL_TYPE_JSON = 1;
    /**
     * 返回为String格式
     */
    public static final int CALL_TYPE_STRING = 2;

    /**
     * OkHttp的Client对象
     * 主要Http请求配置信息
     */
    private static OkHttpClient okHttpClient = getHttpClient();

    /**
     * 通用服务器
     */
    private static Retrofit COMMON_SERVER = createCommonServer();
    /**
     * 字体服务器
     */
    private static Retrofit FONT_SERVER = createFontServer();
    /**
     * S1服务器
     */
    private static Retrofit S1_SERVER = createS1Server();
    /**
     * S2服务器
     */
    private static Retrofit S2_SERVER = createS2Server();
    /**
     * 大数据服务器
     */
    private static Retrofit BIG_DATA_SERVER = createBigdataServer();
    /**
     * 文件服务器
     */
    private static Retrofit FILE_SERVER = createFileServer();

    /**
     * 微信服务器
     * @return
     */
//    private static Retrofit WX_SERVER = createWxServer();


    private static Retrofit createCommonServer() {
        return createRetrofit(BuildConfig.SERVER);
    }

    private static Retrofit createFontServer() {
        return createRetrofit(BuildConfig.FRONT_SERVER);
    }

    private static Retrofit createS1Server() {
        return createRetrofit(BuildConfig.S1_SERVER);
    }

    private static Retrofit createS2Server() {
        return createRetrofit(BuildConfig.S2_SERVER);
    }

    private static Retrofit createBigdataServer() {return createRetrofit(BuildConfig.BIGDATA_SERVER);}

    private static Retrofit createFileServer() {
        return createRetrofit(BuildConfig.FILE_SERVER);
    }

//    private static Retrofit createWxServer() {
//        return createRetrofit(BuildConfig.WX_SERVER);
//    }

    private static Retrofit createRetrofit(String baseurl) {
        return new Retrofit.Builder().baseUrl(baseurl).client(okHttpClient)
                .addConverterFactory(new JsonConverterFactory()).build();
    }

    private static Retrofit createRetrofit(String baseurl, int callType) {
        Converter.Factory factory = null;
        switch (callType) {
            case CALL_TYPE_JSON:
                factory = new JsonConverterFactory();
                break;
            case CALL_TYPE_STRING:
                factory = new StringConverterFactory();
                break;
            default:
                factory = new JsonConverterFactory();
                break;
        }

        return new Retrofit.Builder().baseUrl(baseurl).client(okHttpClient)
                .addConverterFactory(factory).build();
    }

    /**
     * 创建常用服务器指定的api接口的实现
     *
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T createCommon(Class<T> clazz) {
        return COMMON_SERVER.create(clazz);
    }

    /**
     * 创建常用服务器指定的api接口的实现
     *
     * @param clazz
     * @param callType
     * @param <T>
     * @return
     */
    public static <T> T createCommon(Class<T> clazz, int callType) {
        return createRetrofit(BuildConfig.SERVER, callType).create(clazz);
    }

    /**
     * 创建Font服务器指定的api接口的实现
     *
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T createFont(Class<T> clazz) {
        return FONT_SERVER.create(clazz);
    }

    /**
     * 创建Font服务器指定的api接口的实现
     *
     * @param clazz
     * @param callType
     * @param <T>
     * @return
     */
    public static <T> T createFont(Class<T> clazz, int callType) {
        return createRetrofit(BuildConfig.FRONT_SERVER, callType).create(clazz);
    }

    /**
     * 创建S1服务器指定的api接口的实现
     *
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T createS1(Class<T> clazz) {
        return S1_SERVER.create(clazz);
    }

    /**
     * 创建S1服务器指定的api接口的实现
     *
     * @param clazz
     * @param callType
     * @param <T>
     * @return
     */
    public static <T> T createS1(Class<T> clazz, int callType) {
        return createRetrofit(BuildConfig.S1_SERVER, callType).create(clazz);
    }

    /**
     * 创建S2服务器指定的api接口的实现
     *
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T createS2(Class<T> clazz) {
        return S2_SERVER.create(clazz);
    }

    /**
     * 创建S2服务器指定的api接口的实现
     *
     * @param clazz
     * @param callType
     * @param <T>
     * @return
     */
    public static <T> T createS2(Class<T> clazz, int callType) {
        return createRetrofit(BuildConfig.S2_SERVER, callType).create(clazz);
    }

    /**
     * 创建BigData服务器指定的api接口的实现
     *
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T createBigData(Class<T> clazz) {
        return BIG_DATA_SERVER.create(clazz);
    }

    /**
     * 创建BigData服务器指定的api接口的实现
     *
     * @param clazz
     * @param callType
     * @param <T>
     * @return
     */
    public static <T> T createBigData(Class<T> clazz, int callType) {
        return createRetrofit(BuildConfig.BIGDATA_SERVER, callType).create(clazz);
    }

    /**
     * 创建File服务器指定的api接口的实现
     *
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T createFile(Class<T> clazz) {
        return FILE_SERVER.create(clazz);
    }

    /**
     * 创建File服务器指定的api接口的实现
     *
     * @param clazz
     * @param callType
     * @param <T>
     * @return
     */
    public static <T> T createFile(Class<T> clazz, int callType) {
        return createRetrofit(BuildConfig.FILE_SERVER, callType).create(clazz);
    }

    /**
     * 创建指定的url接口的实现
     *
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T createFileUrl(Class<T> clazz, String url) {
        return new Retrofit.Builder().baseUrl(url)
                .addConverterFactory(new StringConverterFactory()).build().create(clazz);
    }

    /**
     * 创建微信服务器指定的api接口的实现
     * @param clazz
     * @param <T>
     * @return
     */
//    public static <T> T createWeixin(Class<T> clazz) {
//        return WX_SERVER.create(clazz);
//    }

    /**
     * 获取常用服务器
     *
     * @return
     */
    private static Retrofit getCommonServer() {
        return COMMON_SERVER;
    }

    /**
     * 获取FONT服务器
     *
     * @return
     */
    public static Retrofit getFontServer() {
        return FONT_SERVER;
    }

    /**
     * 获取S2服务器
     *
     * @return
     */
    private static Retrofit getS1Server() {
        return S1_SERVER;
    }

    /**
     * 获取S2服务器
     *
     * @return
     */
    private static Retrofit getS2Server() {
        return S2_SERVER;
    }

    /**
     * 获取大数据服务器
     *
     * @return
     */
    private static Retrofit getBigDataServer() {
        return BIG_DATA_SERVER;
    }

    /**
     * 获取文件服务器
     *
     * @return
     */
    private static Retrofit getFileServer() {
        return FILE_SERVER;
    }

    /**
     * 获取微信服务器
     * @return
     */
//    private static Retrofit getWxServer() {
//        return WX_SERVER;
//    }

    /**
     * 设置网络配置参数
     *
     * @return
     */
    private static OkHttpClient getHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.readTimeout(AppConfig.TIME_OUT, TimeUnit.SECONDS)
                .connectTimeout(AppConfig.TIME_OUT, TimeUnit.SECONDS)
                .addInterceptor(new CustomInterceptor());
        if (BuildConfig.DEBUG_MODE) {
            HttpLoggingInterceptor bodyInterceptor = new HttpLoggingInterceptor();
            bodyInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            return builder.addInterceptor(bodyInterceptor).build();
        }

        /** 关闭302重定向 **/
        builder.followRedirects(false);
        return builder.build();
    }


    /**
     * 检查是否有网络
     *
     * @param context
     * @return
     */
    public static boolean isNetAvailable(Context context) {
        if (context != null) {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
            if (mNetworkInfo != null)
                return mNetworkInfo.isAvailable();
        }
        return false;
    }
}
