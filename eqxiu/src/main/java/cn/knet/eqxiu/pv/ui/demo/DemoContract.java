package cn.knet.eqxiu.pv.ui.demo;

import cn.knet.eqxiu.pv.base.BasePresenter;
import cn.knet.eqxiu.pv.base.BaseView;
import cn.knet.eqxiu.pv.net.JsonCallback;

/**
 * 作者:  金录
 * 日期:  2016/9/23
 * 描述:
 */
public interface DemoContract {

    interface Presenter extends BasePresenter {

        void getRemoteData(JsonCallback callback);

        void getLocalData();
    }

    interface View extends BaseView {

        void getRemoteDataSucceed();

        void getRemoteDataFail();
    }
}
