package cn.knet.eqxiu.pv.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import cn.knet.eqxiu.pv.R;
import cn.knet.eqxiu.pv.base.BaseCustomView;

/**
 * 作者:    金录
 * 日期:    2016/4/30
 * 描述:    通用标题栏
 */
public class TitleBar extends BaseCustomView {
    @BindView(R.id.ib_back)
    ImageButton ib_back;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_right)
    TextView tv_right;
    @BindView(R.id.ib_right)
    ImageView ib_right;

    private boolean backVisible;
    private String titleText;
    private String rightText;
    private Drawable rightImage;

    public TitleBar(Context context) {
        super(context);
    }

    public TitleBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TitleBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int[] getStyleable() {
        return R.styleable.TitleBar;
    }

    @Override
    protected void initAttributes(TypedArray a) {
        backVisible = a.getBoolean(R.styleable.TitleBar_backVisible, true);
        titleText = a.getString(R.styleable.TitleBar_titleText);
        rightText = a.getString(R.styleable.TitleBar_rightText);
        rightImage = a.getDrawable(R.styleable.TitleBar_rightImage);
    }

    @Override
    protected int getLayout() {
        return R.layout.view_title_bar;
    }

    @Override
    protected void initData(Context context, View view) {
        ib_back.setVisibility(backVisible ? View.VISIBLE : View.GONE);
        tv_title.setText(titleText);

        if (!TextUtils.isEmpty(rightText)) {
            tv_right.setVisibility(View.VISIBLE);
            tv_right.setText(rightText);
        } else {
            tv_right.setVisibility(View.GONE);
        }

        if (null != rightImage) {
            ib_right.setVisibility(View.VISIBLE);
            ib_right.setImageDrawable(rightImage);
        } else {
            ib_right.setVisibility(View.GONE);
        }
    }

    /**
     * 返回按钮的点击事件
     *
     * @param listener
     */
    public void setBackClickListener(View.OnClickListener listener) {
        ib_back.setOnClickListener(listener);
    }

    /**
     * 右边TextView的点击事件
     *
     * @param listener
     */
    public void setRightTextClickListener(View.OnClickListener listener) {
        tv_right.setOnClickListener(listener);
    }

    /**
     * 右边ImageButton的点击事件
     *
     * @param listener
     */
    public void setRightImageButtonClickListener(View.OnClickListener listener) {
        ib_right.setOnClickListener(listener);
    }

    /**
     * 设置标题
     *
     * @param title
     */
    public void setTitle(String title) {
        tv_title.setText(title);
    }

    /**
     * 设置右边TextView内容
     *
     * @param rightText
     */
    public void setRightText(String rightText) {
        tv_right.setVisibility(View.VISIBLE);
        tv_right.setText(rightText);
    }

    /**
     * 设置右边ImageButton图片
     *
     * @param rightImage
     */
    public void setRightImage(Drawable rightImage) {
        ib_right.setVisibility(View.VISIBLE);
        ib_right.setImageDrawable(rightImage);
    }

    /**
     * 设置右边ImageButton图片
     *
     * @param rightImageRes
     */
    public void setRightImage(int rightImageRes) {
        ib_right.setVisibility(View.VISIBLE);
        ib_right.setImageResource(rightImageRes);
    }

    /**
     * 设置右侧文字是否可见
     *
     * @param visibility View.VISIBLE/ View.GONE  (default GONE)
     */
    public void setRightTextVisibility(int visibility) {
        tv_right.setVisibility(visibility);
    }

    /**
     * 设置右侧图片是否可见
     *
     * @param visibility View.VISIBLE/ View.GONE (default GONE)
     */
    public void setRightImgVisibility(int visibility) {
        ib_right.setVisibility(visibility);
    }
}
