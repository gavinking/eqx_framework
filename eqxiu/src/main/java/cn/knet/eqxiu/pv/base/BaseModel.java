package cn.knet.eqxiu.pv.base;

import cn.knet.eqxiu.pv.model.local.LocalEngine;
import cn.knet.eqxiu.pv.model.local.LocalEngineImpl;

/**
 * 作者: 张国荣
 * 日期: 2016/7/26
 * 描述: Model基类
 */
public abstract class BaseModel<E> {

    protected E engine;

    public BaseModel() {
        engine = createEngine();
    }

    /**
     * 获取MODEL
     *
     * @return
     */
    protected abstract E createEngine();

    /**
     * 获取负责远程(网络)数据加载的 Engine
     *
     * @return
     */
    public E getEngine() {
        return engine;
    }

    /**
     * 获取负责本地(数据库，文件)数据加载的 Dao
     *
     * @return
     */
    public LocalEngine getLocalEngine() {
        return LocalEngineImpl.getInstance();
    }
}
