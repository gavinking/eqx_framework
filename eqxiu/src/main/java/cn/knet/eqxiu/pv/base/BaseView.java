package cn.knet.eqxiu.pv.base;

/**
 * 作者:    金录
 * 日期:    2016/7/14
 * 描述:    BaseView
 */
public interface BaseView {
    /**
     * 显示加载进度
     */
    void showLoading();

    /**
     * 取消加载进度
     */
    void dismissLoading();

    /**
     * 显示Error信息
     * @param msg
     */
    void showError(String msg);

    /**
     * 显示网络Error信息
     */
    void showNetError();
}
