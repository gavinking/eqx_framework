package cn.knet.eqxiu.pv.base;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import butterknife.ButterKnife;

/**
 * 作者:    金录
 * 日期:    2016/4/30
 * 描述:    自定义View的基类
 */
public abstract class BaseCustomView extends FrameLayout {

    public BaseCustomView(Context context) {
        super(context);
        initView(context);
    }

    public BaseCustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, getStyleable());
        initAttributes(a);
        initView(context);
        a.recycle();
    }

    public BaseCustomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    /**
     * 初始化布局
     */
    private void initView(Context context) {
        View view = View.inflate(getContext(), getLayout(), this);
        ButterKnife.bind(this, view);
        initData(context, view);
    }

    protected abstract int[] getStyleable();

    protected abstract void initAttributes(TypedArray a);

    protected abstract int getLayout();

    protected abstract void initData(Context context, View view);
}
