package cn.knet.eqxiu.pv.net;

import android.text.TextUtils;

import java.io.IOException;
import java.util.List;

import cn.knet.eqxiu.pv.utils.EqxCookieUtil;
import cn.knet.eqxiu.pv.utils.EqxLog;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * 作者:    金录
 * 日期:    2016/7/18
 * 描述:    Http拦截器
 */
public class CustomInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Request.Builder builder = request.newBuilder();
        /** 添加SessionId**/
        String sessionId = EqxCookieUtil.getSessionidFromPref();
        if (!TextUtils.isEmpty(sessionId)) {
            builder.addHeader("Cookie", "JSESSIONID=" + sessionId);
        }
        builder.addHeader("user-agent", "YiQiXiu Android 2.3.0");
        request = builder.build();

        /**获取SessionId **/
        Response response = chain.proceed(request);
        responseInterceptor(response);
        return response;
    }

    /**
     * 登录、注册、三方登录返回时获取sessionID
     *
     * @param response
     */
    public void responseInterceptor(Response response) {
        //TODO Change to real url
        try {
            String requestUrl = response.request().url().toString();
            if (requestUrl.startsWith("ServerApi.LOGON") || requestUrl.startsWith("ServerApi" +
                    ".THIRD_LOGON")
                    || requestUrl.startsWith("ServerApi.REGISTER_CODE")) {
                List<String> list = response.headers("Set-Cookie");
                if (list == null) return;
                for (String str : list) {
                    if (str.contains("JSESSIONID=")) {
                        String[] cookieParams = str.split(";");
                        if (cookieParams == null) break;
                        for (String param : cookieParams) {
                            param = param.trim();
                            String sessionId;
                            String domain;
                            if (param.startsWith("JSESSIONID")) {
                                sessionId = param.split("=")[1];
                                EqxCookieUtil.addSessionidToPref(sessionId);
                            }
                            if (param.startsWith("Domain")) {
                                domain = param.split("=")[1];
                                EqxCookieUtil.addDomainToPref(domain);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            EqxLog.e("Exception:", e.getMessage());
        }
    }
}
