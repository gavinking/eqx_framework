package cn.knet.eqxiu.pv.model.local;

import cn.knet.eqxiu.pv.model.local.db.DbItem;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * 作者:  金录
 * 日期:  2016/9/26
 * 描述:  本地数据源
 */
public interface LocalEngine {

    /**
     * 保存 String数据 到 缓存中
     *
     * @param key
     * @param value
     */
    void put(String key, String value);

    /**
     * 保存 String数据 到 缓存中
     *
     * @param key
     * @param value
     * @param saveTime 保存时间， 单位： 秒
     */
    void put(String key, String value, int saveTime);

    /**
     * 保存 JSONObject数据 到 缓存中
     *
     * @param key
     * @param value
     */
    void put(String key, JSONObject value);

    /**
     * 保存 JSONObject数据 到 缓存中
     *
     * @param key
     * @param value
     * @param saveTime 保存时间， 单位： 秒
     */
    void put(String key, JSONObject value, int saveTime);

    /**
     * 保存 Serializable数据 到 缓存中
     *
     * @param key
     * @param value
     */
    void put(String key, Serializable value);

    /**
     * 保存 Serializable数据到 缓存中
     *
     * @param key
     * @param value
     * @param saveTime 保存的时间，单位：秒
     */
    void put(String key, Serializable value, int saveTime);

    /**
     * 插入数据
     *
     * @param item
     */
    void add(DbItem item);

    /**
     * /**
     * 删除数据
     *
     * @param key
     */
    void delete(String key);

    /**
     * 更新数据
     *
     * @param item
     */
    void update(DbItem item);

    /**
     * 查询数据
     *
     * @param key
     * @return
     */
    DbItem query(String key);

    /**
     * 检查本地数据是否存在特定key的数据
     *
     * @param key
     * @return
     */
    boolean hasItem(String key);

}
