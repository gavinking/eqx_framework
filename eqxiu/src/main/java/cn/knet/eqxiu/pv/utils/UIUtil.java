package cn.knet.eqxiu.pv.utils;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import cn.knet.eqxiu.pv.app.EqxApp;

/**
 * UI资源工具类
 *
 * @author kejunyao
 * @since 2016年1月19日
 */
public final class UIUtil {

    /**
     * Private Constructor
     */
    private UIUtil() {
    }

    /**
     * 获取主线程的handler
     */
    public static Handler getHandler() {
        return EqxApp.getMainThreadHandler();
    }

    /**
     * 获取ApplicationContext
     *
     * @return
     */
    public static Context getContext() {
        return EqxApp.getAppContext();
    }

    /**
     * 在主线程执行runnable
     */
    public static boolean post(Runnable runnable) {
        return getHandler().post(runnable);
    }

    /**
     * 延时在主线程执行runnable
     */
    public static boolean postDelayed(Runnable runnable, long delayMillis) {
        return getHandler().postDelayed(runnable, delayMillis);
    }


    /**
     * 从主线程looper里面移除runnable
     */
    public static void removeCallbacks(Runnable runnable) {
        getHandler().removeCallbacks(runnable);
    }

    /**
     * 加载布局
     *
     * @param resId layout id
     * @return 布局对应的View
     */
    public static View inflate(int resId) {
        return LayoutInflater.from(getContext()).inflate(resId, null);
    }

    /**
     * 对toast的简易封装线程安全，可以在非UI线程调用
     * Short Toast
     */
    public static void showToast(final int resId) {
        showToast(getString(resId));
    }

    /**
     * Short Toast
     *
     * @param str
     */
    public static void showToast(final String str) {
        post(new Runnable() {
            @Override
            public void run() {
                showShortToast(str);
            }
        });
    }

    /**
     * 延迟Toast
     *
     * @param resId
     * @param delayMillis
     */
    public static void showDelayToast(final int resId, long delayMillis) {
        showDelayToast(getString(resId), delayMillis);
    }

    /**
     * 延迟Toast
     *
     * @param str
     * @param delayMillis
     */
    public static void showDelayToast(final String str, long delayMillis) {
        postDelayed(new Runnable() {
            @Override
            public void run() {
                showToast(str);
            }
        }, delayMillis);
    }

    private static Toast mToast;

    /**
     * Short Toast
     *
     * @param str
     */
    private static void showShortToast(String str) {
        if (TextUtils.isEmpty(str)) return;
        if (mToast == null)
            mToast = Toast.makeText(getContext(), str, Toast.LENGTH_SHORT);
        mToast.setText(str);
        mToast.show();
    }


    /**
     * Long Toast
     *
     * @param resId
     */
    public static void showLongToast(final int resId) {
        showLongToast(getString(resId));
    }

    /**
     * Long Toast
     *
     * @param str
     */
    public static void showLongToast(final String str) {
        post(new Runnable() {
            @Override
            public void run() {
                longToast(str);
            }
        });
    }

    private static Toast mLongToast;

    private static void longToast(String str) {
        if (TextUtils.isEmpty(str)) return;
        if (mLongToast == null)
            mLongToast = Toast.makeText(getContext(), str, Toast.LENGTH_LONG);
        mLongToast.setText(str);
        mLongToast.show();
    }

    /**
     * 弹Toast
     *
     * @param msgResId   消息文本资源ID
     * @param formatArgs 占位字符
     */
    public static void showToast(int msgResId, Object... formatArgs) {
        showToast(getString(msgResId, formatArgs));
    }

    /**
     * 弹Long Toast
     *
     * @param msgResId   消息文本资源ID
     * @param formatArgs 占位字符
     */
    public static void showLongToast(int msgResId, Object... formatArgs) {
        showLongToast(getString(msgResId, formatArgs));
    }

    /**
     * 弹Short Toast
     *
     * @param msg 消息字符串
     */
    public static void showToast(CharSequence msg) {
        showToast(msg.toString());
    }

    /**
     * 弹Long Toast
     *
     * @param msg 消息字符串
     */
    public static void showLongToast(CharSequence msg) {
        showLongToast(msg.toString());
    }

    /**
     * 获取颜色值
     *
     * @param resId 颜色资源ID
     * @return 颜色值
     */
    public static int getColor(int resId) {
        return getContext().getResources().getColor(resId);
    }

    /**
     * 获取颜色状态列表
     *
     * @param resId 颜色状态资源ID
     * @return {@link ColorStateList}
     */
    public static ColorStateList getColorStateList(int resId) {
        return getContext().getResources().getColorStateList(resId);
    }

    /**
     * 获取字符串
     *
     * @param resId 字符串资源ID
     * @return 字符串
     */
    public static String getString(int resId) {
        return getContext().getString(resId);
    }

    /**
     * 获取字符串
     *
     * @param resId      字符串资源ID
     * @param formatArgs 占位符
     * @return 字符串
     */
    private static String getString(int resId, Object... formatArgs) {
        return getContext().getString(resId, formatArgs);
    }

    /**
     * 获取字符串数组
     *
     * @param resId 字符串数组资源ID
     * @return 字符串数组
     */
    public static String[] getStringArray(int resId) {
        return getContext().getResources().getStringArray(resId);
    }

    /**
     * 获取Pixel
     *
     * @param resId dip资源ID
     * @return
     */
    public static int getDimenPixelSize(int resId) {
        return getContext().getResources().getDimensionPixelSize(resId);
    }

    /**
     * 获取Integer值
     *
     * @param resId Integer值资源ID
     * @return
     */
    public static int getInteger(int resId) {
        return getContext().getResources().getInteger(resId);
    }

    /**
     * 获取{@link Drawable}
     *
     * @param resId {@link Drawable} 资源ID
     * @return {@link Drawable}
     */
    public static Drawable getDrawable(int resId) {
        return getContext().getResources().getDrawable(resId);
    }

    /**
     * dip转换px
     */
    public static int dp2px(int dip) {
        final float scale = getContext().getResources().getDisplayMetrics().density;
        return (int) (dip * scale + 0.5f);
    }

    /**
     * pxz转换dip
     */
    public static int px2dp(int px) {
        final float scale = getContext().getResources().getDisplayMetrics().density;
        return (int) (px / scale + 0.5f);
    }

    private static long lastClickTime;

    /**
     * 判断是否为快速重复点击
     * 阀值可以根据需求自己定义
     *
     * @return
     */
    public static boolean isFastClick() {
        long time = System.currentTimeMillis();
        if (time - lastClickTime < 500) {
            return true;
        }
        lastClickTime = time;
        return false;
    }

    /**
     * 获取手机屏幕宽度
     *
     * @return
     */
    public static int getScreenWidth() {
        return getContext().getResources().getDisplayMetrics().widthPixels;
    }

    /**
     * 获取手机屏幕高度
     *
     * @return
     */
    public static int getScreenHeight() {
        return getContext().getResources().getDisplayMetrics().heightPixels;
    }

}
