package cn.knet.eqxiu.pv.ui.demo;

import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.BindString;
import butterknife.BindView;
import cn.knet.eqxiu.library.adapter.BaseViewHolder;
import cn.knet.eqxiu.library.adapter.CommonAdapter;
import cn.knet.eqxiu.library.adapter.animation.ScaleInAnimation;
import cn.knet.eqxiu.library.adapter.decoration.HorizontalDividerItemDecoration;
import cn.knet.eqxiu.library.adapter.listener.OnItemClickListener;
import cn.knet.eqxiu.library.ptr.PtrFrameLayout;
import cn.knet.eqxiu.library.ptr.PtrHandler;
import cn.knet.eqxiu.library.ptr.PullRefreshLayout;
import cn.knet.eqxiu.pv.R;
import cn.knet.eqxiu.pv.base.BaseActivity;
import cn.knet.eqxiu.pv.ui.main.MainActivity;
import cn.knet.eqxiu.pv.utils.UIUtil;
import cn.knet.eqxiu.pv.widgets.TitleBar;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者:  金录
 * 日期:  2016/9/12
 * 描述:  DemoActivity
 */
public class DemoActivity extends BaseActivity<DemoContract.Presenter> implements DemoContract.View {

    @BindView(R.id.rv_demo)
    RecyclerView rvDemo;
    @BindView(R.id.prl)
    PullRefreshLayout prl;
    @BindView(R.id.title)
    TitleBar title;
    @BindString(R.string.app_name)
    String appName;

    private List<String> data;

    private DemoAdapter adapter;

    private int loadMoreTimes;

    @Override
    protected int getRootView() {
        return R.layout.activity_demo;
    }

    @Override
    protected DemoContract.Presenter createPresenter() {
        return new DemoPresenter();
    }

    @Override
    protected void setListener() {
        prl.setPtrHandler(new PtrHandler() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                UIUtil.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        final List<String> resetData = new ArrayList<>();
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        resetData.add("reset");
                        UIUtil.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                adapter.addData(resetData);
                                prl.refreshComplete();
                            }
                        }, 200);

                    }
                }, 2000);
            }
        });

        adapter.setOnLoadMoreListener(new CommonAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                final List<String> newData = new ArrayList<>();
                newData.add("bbbb");
                newData.add("bbbb");
                newData.add("bbbb");
                newData.add("bbbb");
                newData.add("bbbb");
                newData.add("bbbb");
                newData.add("bbbb");
                newData.add("bbbb");
                newData.add("bbbb");
                newData.add("bbbb");
                UIUtil.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        adapter.addData(newData);
//                        adapter.loadComplete();
                    }
                }, 1000);
            }
        });


        rvDemo.addOnItemTouchListener(new OnItemClickListener() {
            @Override
            public void onItemClick(CommonAdapter adapter, View view, int position) {
                UIUtil.showToast(position + "");
                goActivity(MainActivity.class);
            }
        });

        title.setBackClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                Environment.getDataDirectory();
            }
        });
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        title.setTitle("DemoActivity");
        data = new ArrayList<>();
        data.add("adadafd");
        data.add("adadafd");
        data.add("adadafd");
        data.add("adadafd");
        data.add("adadafd");
        data.add("adadafd");
        data.add("adadafd");
        data.add("adadafd");
        data.add("adadafd");
        data.add("adadafd");
        data.add("adadafd");
        data.add("adadafd");
        data.add("adadafd");
        data.add("adadafd");
        data.add("adadafd");
        data.add("adadafd");
        data.add("adadafd");
        data.add("adadafd");
        data.add("adadafd");
        data.add("adadafd");
        adapter = new DemoAdapter(data);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvDemo.setLayoutManager(layoutManager);
        rvDemo.setAdapter(adapter);
        rvDemo.addItemDecoration(new HorizontalDividerItemDecoration.Builder(this).build());
        View header = UIUtil.inflate(R.layout.header_demo);
        adapter.addHeaderView(header);
        View footer = UIUtil.inflate(R.layout.header_demo);
        adapter.addFooterView(footer);

        adapter.openLoadAnimation(new ScaleInAnimation());

    }

    @Override
    public void getRemoteDataSucceed() {

    }

    @Override
    public void getRemoteDataFail() {

    }

    private class DemoAdapter extends CommonAdapter<String> {
        public DemoAdapter(List<String> data) {
            super(data);
        }

        @Override
        protected int getLayoutId() {
            return R.layout.load_more_failed;
        }

        @Override
        protected void convert(BaseViewHolder helper, String item) {
            helper.setText(R.id.tv_prompt, item);
        }
    }

}
