package cn.knet.eqxiu.pv.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import cn.knet.eqxiu.pv.R;
import cn.knet.eqxiu.pv.utils.UIUtil;

public class LoadingPage extends FrameLayout {

    //加载默认的状态
    private static final int STATE_UNLOADED = 1;
    //加载的状态
    private static final int STATE_LOADING = 2;
    //加载失败的状态
    private static final int STATE_ERROR = 3;
    //加载空的状态
    private static final int STATE_EMPTY = 4;
    //加载成功的状态
    private static final int STATE_SUCCEED = 5;

    private View mLoadingView;//转圈的view
    private View mErrorView;//错误的view
    private View mEmptyView;//空的view
//    private View mSucceedView;//成功的view

    private int mState = STATE_UNLOADED;//默认的状态

    private RetryListener mRetryListener;


    public LoadingPage(Context context) {
        super(context);
        init();
    }

    public LoadingPage(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public LoadingPage(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }


    private void init() {
        //初始化状态
        mState = STATE_UNLOADED;

        mLoadingView = createLoadingView();
        if (null != mLoadingView) {
            addView(mLoadingView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        }

        mErrorView = createErrorView();
        if (null != mErrorView) {
            addView(mErrorView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        }

        mEmptyView = createEmptyView();
        if (null != mEmptyView) {
            addView(mEmptyView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        }

//        mSucceedView = createSucceedView();
//        if (null != mSucceedView) {
//            addView(mSucceedView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
//        }
        showSafePagerView();
    }

    /**
     * 确保更新页面在主线程
     */
    private void showSafePagerView() {
        UIUtil.post(new Runnable() {
            @Override
            public void run() {
                showPagerView();
            }
        });
    }

    /**
     * 更新页面显示
     */
    private void showPagerView() {

        setVisibility(mState == STATE_UNLOADED || mState == STATE_SUCCEED ? View.GONE : View.VISIBLE);

        if (null != mLoadingView) {
            mLoadingView.setVisibility(mState == STATE_LOADING ? View.VISIBLE : View.GONE);
        }
        if (null != mErrorView) {
            mErrorView.setVisibility(mState == STATE_ERROR ? View.VISIBLE : View.GONE);
        }
        if (null != mEmptyView) {
            mEmptyView.setVisibility(mState == STATE_EMPTY ? View.VISIBLE : View.GONE);
        }
//
//        if (mSucceedView != null) {
//            mSucceedView.setVisibility(mState == STATE_SUCCEED ? View.VISIBLE : View.GONE);
//        }
    }

    /**
     * 创建数据加载中时的页面
     *
     * @return
     */
    protected View createLoadingView() {
        return UIUtil.inflate(R.layout.loading_page_loading);
    }

    /**
     * 创建加载数据为空时的页面
     *
     * @return
     */
    protected View createEmptyView() {
        View view = UIUtil.inflate(R.layout.loading_page_empty);
        view.findViewById(R.id.iv_empty).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setLoading();
                if (null != mRetryListener) {
                    mRetryListener.onRetry();
                }
            }
        });
        return view;
    }

    /**
     * 创建加载数据失败时的页面
     *
     * @return
     */
    protected View createErrorView() {
        View view = UIUtil.inflate(R.layout.loading_page_error);
        //点击重新加载
        view.findViewById(R.id.iv_reload).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setLoading();
                if (null != mRetryListener) {
                    mRetryListener.onRetry();
                }
            }
        });
        return view;
    }


    /**
     * 设置加载中
     */
    public void setLoading() {
        mState = STATE_LOADING;
        showSafePagerView();
    }

    /**
     * 设置加载失败
     */
    public void setError() {
        mState = STATE_ERROR;
        showSafePagerView();
    }

    /**
     * 设置数据为空
     */
    public void setEmpty() {
        mState = STATE_EMPTY;
        showSafePagerView();
    }

    /**
     * 设置加载成功
     */
    public void setSucceed() {
        mState = STATE_SUCCEED;
        showSafePagerView();
    }

    public void setRetryListener(RetryListener mRetryListener) {
        this.mRetryListener = mRetryListener;
    }

    public interface RetryListener {
        void onRetry();
    }
}
