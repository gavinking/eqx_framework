package cn.knet.eqxiu.pv.model.local;

import cn.knet.eqxiu.pv.app.EqxApp;
import cn.knet.eqxiu.pv.model.local.cache.ACache;
import cn.knet.eqxiu.pv.model.local.db.DbItem;
import cn.knet.eqxiu.pv.model.local.db.EqxDao;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * 作者:  金录
 * 日期:  2016/9/23
 * 描述:  本地数据源
 */
public class LocalEngineImpl implements LocalEngine {
    private static LocalEngineImpl localEngine = new LocalEngineImpl();
    private EqxDao eqxDao;
    private ACache aCache;

    private LocalEngineImpl() {
        eqxDao = EqxDao.getInstance();
        aCache = ACache.get(EqxApp.getAppContext());
    }

    /**
     * 获取BaseDao实例
     *
     * @return
     */
    public static LocalEngineImpl getInstance() {
        return localEngine;
    }

    @Override
    public void put(String key, String value) {
        aCache.put(key, value);
    }

    @Override
    public void put(String key, String value, int saveTime) {
        aCache.put(key, value, saveTime);
    }

    @Override
    public void put(String key, JSONObject value) {
        aCache.put(key, value);
    }

    @Override
    public void put(String key, JSONObject value, int saveTime) {
        aCache.put(key, value, saveTime);
    }

    @Override
    public void put(String key, Serializable value) {
        aCache.put(key, value);
    }

    @Override
    public void put(String key, Serializable value, int saveTime) {
        aCache.put(key, value, saveTime);
    }

    /**
     * 插入数据
     *
     * @param item
     */
    @Override
    public void add(DbItem item) {
        eqxDao.add(item);
    }

    /**
     * /**
     * 删除数据
     *
     * @param key
     */
    @Override
    public void delete(String key) {
        eqxDao.delete(key);
    }

    /**
     * 更新数据
     *
     * @param item
     */
    @Override
    public void update(DbItem item) {
        eqxDao.update(item);
    }

    /**
     * 查询数据
     *
     * @param key
     * @return
     */
    @Override
    public DbItem query(String key) {
        return eqxDao.query(key);
    }

    /**
     * 检查本地数据是否存在特定key的数据
     *
     * @param key
     * @return
     */
    @Override
    public boolean hasItem(String key) {
        return eqxDao.hasItem(key);
    }
}
