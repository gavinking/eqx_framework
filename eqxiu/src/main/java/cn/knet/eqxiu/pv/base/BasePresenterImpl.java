package cn.knet.eqxiu.pv.base;

import cn.knet.eqxiu.pv.model.local.LocalEngine;

/**
 * 作者:    金录
 * 日期:    2016/4/30
 * 描述:    Presenter基类
 */
public abstract class BasePresenterImpl<V extends BaseView, M extends BaseModel> implements BasePresenter {

    private boolean viewAttached;
    protected V mView;
    protected M mModel;

    public BasePresenterImpl() {
        mModel = createModel();
    }

    @Override
    public void attachView(BaseView view) {
        mView = (V) view;
        viewAttached = true;
    }

    @Override
    public void detachView() {
        viewAttached = false;
        mView = null;
        mModel = null;
    }

    /**
     * 创建对应的model
     *
     * @return
     */
    protected abstract M createModel();

    /**
     * 判断View是否Attach到Presenter，
     *
     * @return
     */
    public boolean isViewAttached() {
        return viewAttached;
    }

    /**
     * 获取本地数据加载 Engine
     * mModel.getLocalEngine()简写
     *
     * @return
     */
    protected LocalEngine getLocalEngine() {
        return mModel.getLocalEngine();
    }
}
