package cn.knet.eqxiu.pv.ui.main;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import butterknife.BindView;
import cn.knet.eqxiu.pv.R;
import cn.knet.eqxiu.pv.base.BaseActivity;
import cn.knet.eqxiu.pv.base.BasePresenterImpl;

import java.util.List;

/**
 * 作者:  金录
 * 日期:  2016/9/12
 * 描述:  主页面Activity
 */
public class MainActivity extends BaseActivity {

    @BindView(R.id.srl)
    SwipeRefreshLayout srl;

    private List<String> data;

    @Override
    protected int getRootView() {
        return R.layout.activity_main;
    }

    @Override
    protected BasePresenterImpl createPresenter() {
        return null;
    }

    @Override
    protected void setListener() {
    }

    @Override
    protected void initData(Bundle savedInstanceState) {

    }
}
