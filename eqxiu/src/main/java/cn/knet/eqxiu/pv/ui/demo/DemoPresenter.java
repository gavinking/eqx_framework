package cn.knet.eqxiu.pv.ui.demo;

import cn.knet.eqxiu.pv.base.BasePresenterImpl;
import cn.knet.eqxiu.pv.model.DemoModel;
import cn.knet.eqxiu.pv.model.local.db.DbItem;
import cn.knet.eqxiu.pv.net.JsonCallback;

/**
 * 作者:  金录
 * 日期:  2016/9/23
 * 描述:
 */
public class DemoPresenter extends BasePresenterImpl<DemoContract.View, DemoModel>
        implements
        DemoContract.Presenter {
    @Override
    protected DemoModel createModel() {
        return new DemoModel();
    }

    @Override
    public void getRemoteData(JsonCallback callback) {
        mModel.getEngine().getGoods().enqueue(callback);
        mView.getRemoteDataSucceed();
    }

    @Override
    public void getLocalData() {
//        mModel.getLocalEngine().getLocalData();
//        getLocalEngine().getLocalData();
//        DbItem item = mModel.getLocalEngine().query("Demo_key");
        DbItem item = getLocalEngine().query("Demo_key");
        mModel.test();
    }
}
