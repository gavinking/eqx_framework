package cn.knet.eqxiu.pv.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import cn.knet.eqxiu.pv.utils.UIUtil;
import cn.knet.eqxiu.pv.widgets.LoadingDialog;

/**
 * 作者:    金录
 * 日期:    2016/4/30
 * 描述:    Fragment基类
 */
public abstract class BaseFragment<P extends BasePresenterImpl> extends Fragment implements BaseView {

    protected BaseActivity mActivity;
    protected P mPresenter;
    private LoadingDialog loadingDia;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mActivity = (BaseActivity) getActivity();
        preLoad();
        return UIUtil.inflate(getRootView());
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        mPresenter = createPresenter();
        if (null != mPresenter) mPresenter.attachView(this);
        restoreFragmentState(savedInstanceState);
        initData();
        setListener();
    }

    @Override
    public void onDestroyView() {
        if (loadingDia != null)
            loadingDia.dismiss();
        loadingDia = null;
        if (null != mPresenter) {
            mPresenter.detachView();
            mPresenter = null;
        }
        super.onDestroyView();
    }


    /**
     * 显示加载进度对话框
     */
    @Override
    public void showLoading() {
        showLoading("加载中…");
    }

    /**
     * 显示加载进度对话框
     */
    public void showLoading(String info) {
        try {
            if (loadingDia == null)
                loadingDia = new LoadingDialog();

            Bundle bundle = new Bundle();
            bundle.putString("msg", info);
            loadingDia.setArguments(bundle);
            loadingDia.show(mActivity.getFragmentManager(), "loading");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 隐藏加载进度对话框
     */

    @Override
    public void dismissLoading() {
        try {
            if (mActivity.isFinishing()) return;
            if (loadingDia != null && loadingDia.isAdded())
                loadingDia.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showError(String msg) {
        UIUtil.showToast(msg);
    }

    @Override
    public void showNetError() {
        UIUtil.showToast("网络链接异常，请稍后重试");
    }

    /**
     * 预加载
     */
    protected void preLoad() {

    }

    /**
     * 获取布局id
     *
     * @return
     */
    protected abstract int getRootView();

    /**
     * 创建View对应的Presenter
     *
     * @return
     */
    protected abstract P createPresenter();

    /**
     * 设置监听
     */
    protected abstract void setListener();

    /**
     * 设置数据
     */
    protected abstract void initData();

    /**
     * 恢复viewState
     *
     * @param savedInstanceState
     */
    protected void restoreFragmentState(Bundle savedInstanceState) {

    }

    /**
     * 跳转到其他Activity
     *
     * @param activity
     */
    protected void goActivity(Class<?> activity) {
        Intent intent = new Intent(mActivity, activity);
        mActivity.startActivity(intent);
    }
}
