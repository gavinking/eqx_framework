package cn.knet.eqxiu.pv.app;

import android.content.Context;
import android.os.Looper;
import android.widget.Toast;

import java.io.File;
import java.io.RandomAccessFile;
import java.lang.Thread.UncaughtExceptionHandler;

import cn.knet.eqxiu.pv.R;
import cn.knet.eqxiu.pv.sys.ActivitiesManager;

/**
 * UncaughtException处理类,当程序发生Uncaught异常的时候,有该类来接管程序,并记录发送错误报告.
 *
 * @author user
 */
public class CrashHandler implements UncaughtExceptionHandler {

    // 系统默认的UncaughtException处理类
    private Thread.UncaughtExceptionHandler mDefaultHandler;
    // CrashHandler实例
    private static CrashHandler INSTANCE = new CrashHandler();
    // 程序的Context对象
    private Context mContext;


    /**
     * 保证只有一个CrashHandler实例
     */
    private CrashHandler() {
    }

    /**
     * 获取CrashHandler实例 ,单例模式
     */
    public static CrashHandler getInstance() {
        return INSTANCE;
    }

    /**
     * 初始化
     *
     * @param context
     */
    public void init(Context context) {
        mContext = context;
        // 获取系统默认的UncaughtException处理器
        mDefaultHandler = Thread.getDefaultUncaughtExceptionHandler();
        // 设置该CrashHandler为程序的默认处理器
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    /**
     * 当UncaughtException发生时会转入该函数来处理
     */
    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
        if (!handleException(ex) && mDefaultHandler != null) {
            // 如果用户没有处理则让系统默认的异常处理器来处理
            mDefaultHandler.uncaughtException(thread, ex);
        } else {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
            }
            // 退出程序
            ActivitiesManager.exitApp();
//            EqxApplication.getInstance().exit();
        }
    }

    /**
     * 自定义错误处理,收集错误信息 发送错误报告等操作均在此完成.
     *
     * @param ex
     * @return true:如果处理了该异常信息;否则返回false.
     */
    private boolean handleException(Throwable ex) {

        if (ex == null) {
            return false;
        }
        // 使用Toast来显示异常信息
        new Thread() {
            @Override
            public void run() {
                Looper.prepare();
                Toast.makeText(mContext, R.string.program_crash,
                        Toast.LENGTH_LONG).show();
                Looper.loop();
            }
        }.start();

        ex.printStackTrace();

        // 保存日志文件
        //saveCrashInfo2File(ex);
        //收集编辑界面崩溃的场景信息
        collectCrashedSceneData(mContext);
        return true;
    }


    private void collectCrashedSceneData(Context ctx) {

//        ActivityManager am = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
//        ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
//
//            EditActivity mActivity = (EditActivity) ActivitiesManager.getActivity(EditActivity.class);
//            if (null != mActivity) {
//                long sceneId = mActivity.getSceneId();
//                boolean isPcScene = mActivity.isPcScene;
//                Scene scene = mActivity.getScene();
//                List<PageBean> pageList = mActivity.getPageList();
//                JSONObject data = new JSONObject();
//                try {
//                    data.put(SyncStateContract.Constants.JSON_ID, sceneId);
//                    data.put(Constants.JSON_PC_SCENE, isPcScene);
//                    data.put(Constants.JSON_SCENE, GsonUtil.parseString(scene));
//                    data.put(Constants.JSON_ELEMENTS, GsonUtil.parseString(pageList));
//                } catch (JSONException e) {
//                }
//
//                String filePath = Constants.IMAGE_DIRECTORY;
//                String fileName = Constants.CRASH;
//
//                writeTxtToFile(data.toString(), filePath, fileName);
//            }
    }

    // 将字符串写入到文本文件中
    private void writeTxtToFile(String strcontent, String filePath, String fileName) {

        String strFilePath = filePath + fileName;
        String strContent = strcontent + "\n";
        try {
            File file = new File(strFilePath);
            if (!file.exists()) {
                file.getParentFile().mkdirs();
                file.createNewFile();
                RandomAccessFile raf = new RandomAccessFile(file, "rwd");
                raf.seek(file.length());
                raf.write(strContent.getBytes());
                raf.close();
            }
        } catch (Exception e) {

        }
    }
}