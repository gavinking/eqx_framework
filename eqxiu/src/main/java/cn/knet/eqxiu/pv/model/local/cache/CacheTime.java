package cn.knet.eqxiu.pv.model.local.cache;

/**
 * 作者:  金录
 * 日期:  2016/11/4
 * 描述:  缓存时间常量
 */

public interface CacheTime {
    public static final int TIME_MINUTE = 60;
    public static final int TIME_HOUR = TIME_MINUTE * 60;
    public static final int TIME_DAY = TIME_HOUR * 24;
}
