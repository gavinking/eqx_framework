# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in D:\Android\adt\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}



-keepattributes InnerClasses
-keep class **.R$* {
    <fields>;
}


-optimizationpasses 5
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontpreverify
-verbose
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*
-ignorewarnings

-keepattributes SourceFile,LineNumberTable
-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference
-keep public class com.android.vending.licensing.ILicensingService

-keepclasseswithmembernames class * {
    native <methods>;
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keepclassmembers class * extends android.app.Activity {
   public void *(android.view.View);
}

-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}


#不混淆泛型
-keepattributes Signature
#不混淆Serializable
-keepnames class * implements java.io.Serializable{*;}

-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}

## butterknife begin
-keep class butterknife.** { *; }
-dontwarn butterknife.internal.**
-keep class **$$ViewBinder { *; }
-keepclasseswithmembernames class * {
    @butterknife.* <fields>;
}
-keepclasseswithmembernames class * {
    @butterknife.* <methods>;
}
## butterknife end

## eventbus begin
-keepattributes *Annotation*
-keepclassmembers class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }

# Only required if you use AsyncExecutor
-keepclassmembers class * extends org.greenrobot.eventbus.util.ThrowableFailureEvent {
    <init>(java.lang.Throwable);
}
## eventbus end

#Retrofit begin
# Platform calls Class.forName on types which do not exist on Android to determine platform.
-dontnote retrofit2.Platform
# Platform used when running on RoboVM on iOS. Will not be used at runtime.
-dontnote retrofit2.Platform$IOS$MainThreadExecutor
# Platform used when running on Java 8 VMs. Will not be used at runtime.
-dontwarn retrofit2.Platform$Java8
# Retain generic type information for use by reflection by converters and adapters.
-keepattributes Signature
# Retain declared checked exceptions for use by a Proxy instance.
-keepattributes Exceptions
#Retrofit end

## okhttp  begin
-dontwarn okhttp3.**
-keep class okhttp3.** { *; }
## okhttp  end

#保持Webview不被混淆
-keep class * extends android.webkit.WebView
#保持Webview JS交互不被混淆
-keepattributes *JavascriptInterface*
-keep class cn.knet.eqxiu.modules.extension.view.**{*;}
-keepclassmembers class * extends android.webkit.WebViewClient {*;}
-keepclassmembers class cn.knet.eqxiu.modules.main.view.MainActivity{
    public static boolean *;
}

#############################################  libs ####################################################
#百度--统计
#-libraryjars libs/android_api_3.6.jar
-keep class com.baidu.**{*;}
-dontwarn com.com.baidu.**

#阿里--支付宝
#-libraryjars libs/alipaySdk-20160516.jar
-keep class org.json.alipay.**{*;}
-dontwarn org.json.alipay.**
-keep class com.alipay.**{*;}
-dontwarn com.alipay.**
-keep class com.ta.utdid2.**{*;}
-dontwarn com.ta.utdid2.**
-keep class com.ut.device.**{*;}
-dontwarn com.ut.device.**

#腾讯公共类
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep class com.tencent.**{*;}
-keep class com.qq.e.**{*;}
-keep class ct.**{*;}
#腾讯 信鸽
#-libraryjars libs/Xg_sdk_v2.43_20160308_1031.jar
#腾讯地图2Dsdk
#-libraryjars libs/TencentMapSDK_Raster_v1.1.2.16281.jar
#腾讯地图检索sdk
#-libraryjars libs/TencentSearch_v1.1.2.16095.jar
-keep class com.google.gson.examples.android.model.** {*;}
#腾讯地图定位SDK
#-libraryjars libs/TencentLocationSDK_v4.4.6_r206631_151119_1441.jar
-keep class com.tencent.**{*;}

#腾讯--广点通
#-libraryjars libs/GDTUnionSDK.4.8.524.min.jar
-keep class MTT.ThirdAppInfoNew{*;}
-keep class com.qq.e.**{*;}

#腾讯--QQ互联
#-libraryjars libs/open_sdk_r5043_lite.jar
-keep class com.tencent.**{*;}

#腾讯--微信支付
#-libraryjars libs/libammsdk.jar

#新浪微博
#-libraryjars libs/weibosdkcore_v3.0.1.jar
-keep class com.sina.**{*;}
-dontwarn com.sina.**

#七牛--文件上传
#-libraryjars libs/qiniu-android-sdk-7.2.0.jar
-keep class com.qiniu.android.**{*;}
-dontwarn com.qiniu.android.**
#-libraryjars libs/happy-dns-0.2.7.jar
-keep class com.qiniu.android.dns.**{*;}
-dontwarn com.qiniu.android.dns.**