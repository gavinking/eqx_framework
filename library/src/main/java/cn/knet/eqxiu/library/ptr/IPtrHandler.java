package cn.knet.eqxiu.library.ptr;

import android.view.View;

public interface IPtrHandler {

    /**
     * Check can do refresh or not. For example the content is page_empty or the first child is in view.
     * <p/>
     */
    public boolean checkCanDoRefresh(final PtrFrameLayout frame, final View content, final View header);

    /**
     * When refresh begin
     *
     * @param frame
     */
    public void onRefreshBegin(final PtrFrameLayout frame);
}